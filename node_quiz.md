# Node Quiz
**Goals:** (1) to test and improve your Git skills, while (2) gently introducing you to Node.js.

By the end of this document, you should be able to:

1. Git - work with remote repositories, branches, and manage changes.
2. Node - Create a basic node.js project; work with NPM: project dependencies, testing, meta data; write a working console app.
3. Markdown - Start a readme.md for your project.

_**Time required:** 25 minutes_

## Prerequisites
These instructions are designed for Windows Powershell (optionally, cmd.exe might work, too). You will need git and node installed.

- http://git-scm.com/downloads
- http://nodejs.org/download/

# Let's Begin

## 1. Let's make a Node.js project
Open a powershell console, and change directories to a temporary directory, like c:\temp\. If one doesn't exist, you can
create it with the mkdir command.

1. `cd` into that temp directory, and make a project folder called 'nodequiz.'
2. Initialize a git repository in that directory. (*Remember*: This is a quiz, so the required commands will not be included, here).
3. Create a new file called 'quiz.js' using your favorite editor (notepad, right?), and copy/paste the following javascript into it:

		function sumAll(arr) {
	
		}
	
		console.log(sumAll([1,2,3,4,5]));
	
	
4. Now, fill in the function sumAll so it returns the sum of the numbers in that array.
5. Run the quiz.js file from the powershell by typing `node quiz.js`.
6. When you're satisfied that quiz.js is working, correctly, tell git to begin tracking your new, quiz.js file, and then commit your code.
	- *Grade yourself:* After the above, Git should say something like:
	
			1 file changed, N insertions(+)
			create mode 100644 quiz.js	
	
	
7. Congratulations, you now have a working, Node.js project.

## 2. Keeping your safe
Javascript on the command line - Not so bad, right? Anyway, having a local history of your code changes is nice, but what if your hard drive toasts itself?
Let's keep your work safe.

1. Log into https://git.its.uiowa.edu/projects/ITSADPLAY, a playground for our Git/Node learning process, and create a new repository called [hawkid]quiz.
2. Find the URL of your project in Stash, and copy it.
3. From the shell, use git to add a remote called 'origin' to your local git repository (right-clicking the powershell window will paste the URL at the current cursor location).
4. Use git to push your latest changes to Stash.
5. In your browser, refresh the Stash page. If you see your source code, you were successful. Congratulations, your code is now safe. :-)

## 3. Other remotes, coworkers, and conflict-free merging.
Ok, now, your very helpful coworker has been hacking away on some tests for your new, mission-critical `sumAll` function.
They've already created a repository in Stash, and added some tests. Let's grab those tests, and make sure they're passing.

1. From the shell, use git to add a remote repository to your local store called 'helpful-tim'. The URL is:
	- https://git.its.uiowa.edu/scm/itsadpg/nodequiz.git
2. Use git to pull down the code from helpful-tim. There should be no conflicts, since no edits were made to the same files. The result
   will be like copying and pasting the helpful-tim files into your nodequiz directory.
3. To run the tests, we need a test framework. Let's use NPM, the Node Package Manager, to install one: `npm install nodeunit`.
4. List the directory, and notice our shiny, new `node_modules` directory.
4. We also need to make our function available to the tests. Add the following line of code to the end of quiz.js: `module.exports['sumAll'] = sumAll;`. Save the file.
5. Now, we can use node to run Tim's tests: `node .\node_modules\nodeunit\bin\nodeunit .\tests\`
6. If your sumAll function was written according to the specifications you were provided, you should see nothing but green, passing tests.
7. No such luck? Fix your sumAll function. (Hint: to check if an array element is a number, in Javascript, use `typeof arr[i] === 'number'`).
8. Tests passing, now? Great! Commit your work.

## 4. Let's make testing easier
Node lets you specify a package.json file, in your project, which will make many common tasks easier, such as running tests,
and specifying dependencies. Fortunately, our helpful coworker already has a barebones package.json, along with some shiny,
new tests, in a different repo he created (he's kind of wasteful like that).

1. Use git to change the URL of the helpful-tim remote. This is a less-common task, so I will give you the command:
	- `git remote set-url helpful-tim https://git.its.uiowa.edu/scm/itsadpg/nodequiz-part2.git`
	- Hint: `git remote help`, or, `git remote asdfasdf`, if you ever forget a particular subcommand.
2. Now, pull down the changes from helpful-tim. You should get a new file, `package.json`, and some updates to `tests/sumall-tests.js`.
3. Open package.json in an editor.
4. Take a moment to familiarize yourself with its structure (it's normal JSON), then add `"test" : "node .\node_modules\nodeunit\bin\nodeunit .\tests\"`
   to the "scripts" section, and save the file.
5. Now, from the shell, run `npm test`. NPM looks in your package.json to learn how to run tests. You should see the results of your unit tests.
6. Pretty neat, eh?
7. Except your sumAll is still broken. :-) Fix it, then commit.

## 5. Stashing and nomnom
_Scenario:_ Your customer has asked for enhancements. While working on the enhancements, you're asked to fix a bug. How best to handle?

1. For the next iteration of your product, the server admins need it to work from the shell. To this end, a library called nomnom was already added to
   package.json. Add a new file called 'cli.js' (command line interface) to the root directory, and paste the following code:
```
var nomnom = require('nomnom');
var adder = require('./quiz.js').sumAll;

var opts =
	nomnom
	.option('numlist', {
		default: [],
		flag: true,
		list: true,
		position: 0,
		help: '[n1 [n2 [n3 ...]]] (A space-separated list of numbers)'
	})
	.parse();

exports.main = function() {
	console.log("Summing list: %j", opts['numlist']);
	console.log("Result: ", adder(opts['numlist']));
};

if (require.main === module)
	exports.main();
	
```

2. Oops, just got the email about the bug. Save the file, and stash the changes.
3. Switch the helpful-tim repo to part3, where a test covering the bug may be found:
	- `git remote set-url helpful-tim https://git.its.uiowa.edu/scm/itsadpg/nodequiz-part3.git`
4. Pull down the changes in helpful-tim, and make the test pass, fixing the bug.
5. Commit the changes.
6. Pop your cli.js work off the stash, and commit it.
7. Test cli.js by typing `node cli.js 1 2 3 four 5`.
8. You also want to maintain an overview of your project. Create a file called `readme.md` and add the following code:
```
	# My Project Title
	Here are some things to remember:
	1. sumAll works with spelled-out numerals
	2. keep the tests passing, please.
```
Then commit the readme, and push it up to origin. Finally, go see how pretty it looks in Stash. :-)